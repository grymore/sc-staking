require('dotenv').config()

const ganache = require('ganache')
// const { toBN } = require('web3').utils

const accounts = require('./config/accounts.js').dev.map((account) => {
  return { secretKey: '0x' + account, balance: 10000000000000000000000 }
})
const option = {
  fork: {
    url: process.env.FORKING_BSCSCAN_RPC,
    blockNumber: parseInt(process.env.FORKING_BSCSCAN_RPC_BLOCKNUM)
  },
  port: parseInt(process.env.FORKING_BSCSCAN_RPC_PORT),
  accounts,
  debug: true
}
const server = ganache.server(option)
server.listen(option.port, async (err) => {
  if (err) throw err

  console.log(`Ganache listening on port ${option.port}...`)
  const provider = server.provider
  const accounts = await provider.request({ method: 'eth_accounts', params: [] })
  console.log(accounts)

  // todo: how to logging PK?
})
